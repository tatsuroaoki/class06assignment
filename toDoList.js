// If an li element is clicked, toggle the class "done" on the <li>
const checkComplete = function(e) {
	let $this = $(this).parent();
	$this.toggleClass('done');
}

// If a delete link is clicked, delete the li element / remove from the DOM
const deleteItem = function(e) {
	let $this = $(this).parent();
	$this.remove();
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
//const itemMove = document.querySelectorAll('a.move');
//console.log(itemMove);
//console.log(itemMove[0])
//console.log(itemMove[1])

const moveLists = function(e) {
	let $this = $(this);
	let $thisList = ($(this).parent())
	const $todayList = $('.today-list');
	const $laterList = $('.later-list');

	if ($thisList.parent().hasClass('today-list')) {
		$thisList.remove();
		$this.removeClass('toLater');
		$this.addClass('toToday');
		$this.html('Move to Today');
		$laterList.append($thisList);
	} else if ($thisList.parent().hasClass('later-list')) {
		$thisList.remove();
		$this.removeClass('toToday');
		$this.addClass('toLater');
		$this.html('Move to Later');
		$todayList.append($thisList);
	}
	$this.prev().on('click', checkComplete);
	$this.next().on('click', deleteItem);
	$this.on('click', moveLists);
}

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
	e.preventDefault();
	const $input = $(this).prev();
	const $text = $input.val(); // use this text to create a new <li>
	const text = $text;
	//
	// Finish function here
	//
	// Create elements for new item
	//	const newListItem = document.createElement('li');
	const $this = $(this);
	const $addListCategory = $(this).parent().prev();
	let $newLi = $('<li>');
	let $newSpan = $('<span>');
	let $newA = $('<a>');
	let $newAA = $('<a>');

	$addListCategory.append($newLi);
	const $newListItem = $addListCategory.children().last();

	// Add <span> element
	$newListItem.append($newSpan);
	let $newElement = $newListItem.children().last();
	$newElement.html(text);

	// Add <a> element
	$newListItem.append($newA);
	$newElement = $newListItem.children().last();
	if ($addListCategory.hasClass('today-list')) {
		$newElement.addClass('move toLater').text('Move to Later');
	} else {
		$newElement.addClass('move toToday').text('Move to Today');
	}

	// Add <a> element
	$newListItem.append($newAA);
	$newElement = $newListItem.children().last();
	$newElement.addClass('delete');
	$newElement.text('Delete');

	// Add Event Listeners to new items
	const $innerElement = $this.parent().prev().children().last().children();
	console.log($innerElement);
	$innerElement.first().on('click', checkComplete);
	$innerElement.first().next().on('click', moveLists);
	$innerElement.last().on('click', deleteItem);
}

// Add this as a listener to the two Add links
const $addItemArray = $('.add-item');
$addItemArray.on('click', addListItem);

// Listener for done
const $itemArray = $('span');
$itemArray.on('click', checkComplete);

// Listener for delete
const $deleteItemArray = $('a.delete');
$deleteItemArray.on('click', deleteItem);

// Listener for moving
const $moveList = $('.move');
$moveList.on('click', moveLists);