// IN-CLASS EXERCISE #1: WHERE'S THE POINTER?
const grid = document.getElementsByTagName('td');
console.log(grid);
for (let i = 0; i < grid.length; i++) {
	grid[i].addEventListener('click',function(e) {
		let td = e.target;
		td.innerHTML = `(${e.clientX}, ${e.clientY})`;
	})
}

// IN-CLASS EXERCISE #2: CAR CONSTRUCTOR
const Car = function (model) {
	this.currentSpeed = 0;
	this.model = model;
}

Car.prototype.accelerate = function() {
	return this.currentSpeed += 1;
}

Car.prototype.brake = function() {
	return this.currentSpeed -= 1;
}

Car.prototype.toString = function() {
	return `Your ${this.model} is going at ${this.currentSpeed} mph`;
}

// IN-CLASS EXERCISE #3: CRUD RETURNS - PART 1
// See jQueryDomCrud.js

// IN-CLASS EXERCISE #4: CRUD RETURNS - PART 2
// See jQueryDomCrud.js

// IN-CLASS EXERCISE #5: CRUD RETURNS - PART 3
// See jQueryDomCrud.js