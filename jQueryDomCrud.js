$( document ).ready(function() {

	// Create a new <a> element containing the text "Buy Now!" 
	// with an id of "cta" after the last <p>


	// Access (read) the data-color attribute of the <img>,
	// log to the console
	const $img = jQuery('img');
	const img = $img[0];
	console.log(img.dataset.color);	


	// Update the third <li> item ("Turbocharged"), 
	// set the class name to "highlight"
	let li3 = document.getElementsByTagName('li')[2];
	const $li3 = li3;
	$('li').addClass('highlight');


	// Create a new <a> element containing the text "Buy Now!" with an id of "cta" after the last <p>
	const $main = $('main');
	const $newA = $('<a href>');

	$main.append($newA);
	let $lastA = $('a').last();
	$lastA.attr('href','https://www.audi.com/en.html');
	$lastA.attr('id', 'cta');
	$lastA.html('Buy Now!');

	// Remove (delete) the last paragraph
	// (starts with "Available for purchase now…")
	$lastP = $('p').last();
	$lastP.remove();
});